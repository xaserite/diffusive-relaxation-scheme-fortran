module module_DRS
  implicit none
  private
    real(8),external,private :: psi
  type, public :: DRS
    real(8),allocatable,dimension(:,:):: ro, j ! mass density, flux function arrays
    real(8),allocatable,dimension(:)  :: x 	! uniform spatial grid point vector
    real(8) :: T, dx, dt 					! discretization parameters
    integer :: x_N, t_N , it_t	! discretization parameters
    real(8) :: eps, psi, sqrt_psi  ! scaling parameters
    real(8) :: A, B								! linear collision (system defining) parameters
    real(8) :: lambda, l1, l2			! cell CFL numbers
  contains
    procedure :: init => DRS_init
    procedure :: fromFile => DRS_fromFile
    procedure :: compute => DRS_compute
    procedure :: toFile => DRS_toFile
    procedure :: roToGnuplot => DRS_roToGnuplot
    procedure :: jToGnuplot => DRS_jToGnuplot
    procedure :: free => DRS_free
  end type DRS
contains
! class creators
subroutine DRS_init(this,ro_0,j_0,x_0,T,t_N,eps,A,B)
    class(DRS) :: this
    real(8),dimension(:),intent(in) :: ro_0, j_0, x_0
    real(8),intent(in) :: T, eps, A, B
    integer,intent(in) :: t_N
    this%x_N = size(x_0)
    this%x = x_0
    this%T = T
    this%t_N = t_N
    this%eps = eps
    this%A = A
    this%B = B
    this%it_t = 1
    this%dt = T/t_N
    this%dx = x_0(2) - x_0(1)
    call DRS_init_functions(this,ro_0,j_0)
    call DRS_calc_lambdas(this)
end subroutine DRS_init
subroutine DRS_init_functions(this,ro_0,j_0)
    class(DRS) :: this
    real(8),dimension(:),intent(in) :: ro_0, j_0
    allocate (this%ro(this%x_N,this%t_N))
    allocate (this%j(this%x_N,this%t_N))
    this%ro(:,1) = ro_0
    this%j(:,1) = j_0
end subroutine
subroutine DRS_calc_lambdas(this)
class(DRS) :: this
    real(8) :: a
    this%psi = psi(this%eps)
    this%sqrt_psi = sqrt(this%psi)
    this%lambda = this%sqrt_psi * this%dt/this%dx
    a = 2*this%eps * this%eps
    this%l1 = a/(a+this%B*this%dt)
    this%l2 = this%dt/(a+this%B*this%dt)
    end subroutine
!
! splitting approach to solver
subroutine DRS_compute(this)
class(DRS) :: this
    do while(this%it_t<this%t_N-1)
        call DRS_convection_step(this)
        this%it_t = this%it_t+1
        call DRS_relaxation_step(this)
    end do
end subroutine
!
! Upwind scheme step for diagonalized system
subroutine DRS_convection_step(this)
class(DRS) :: this
    real(8) :: ro_l,ro_m,ro_r, j_l,j_m,j_r
    real(8),dimension(2) :: ro_nbrs, j_nbrs
    integer :: k
    do k = 1,this%x_N
        ro_nbrs = DRS_ro_nbrs(this,k)
        ro_l = ro_nbrs(1)
        ro_m = this%ro(k,this%it_t)
        ro_r = ro_nbrs(2)
        j_nbrs = DRS_j_nbrs(this,k)
        j_l = j_nbrs(1)
        j_m = this%j(k,this%it_t)
        j_r = j_nbrs(2)
        this%ro(k,this%it_t+1) = ro_m + .5* this%lambda * (ro_l - 2*ro_m + ro_r)&
                        - .5* this%dt / this%dx * (j_r - j_l)
        this%j(k,this%it_t+1) = j_m + .5* this%lambda * (j_l - 2*j_m + j_r)&
                        - .5* this%lambda * this%sqrt_psi * (ro_r-ro_l)
    end do
end subroutine
!
! relaxation step: backwards Euler
subroutine DRS_relaxation_step(this)
class(DRS) :: this
    real(8) :: ro_l,ro_m,ro_r, j_m, a
    real(8),dimension(2) :: ro_nbrs
    integer :: k
    a = 1 - this%eps*this%eps * this%psi
    do k = 1,this%x_N
        ! preparation
        ro_nbrs = DRS_ro_nbrs(this,k)
        ro_l = ro_nbrs(1)
        ro_m = this%ro(k,this%it_t)
        ro_r = ro_nbrs(2)
        j_m = this%j(k,this%it_t)
        ! ro constant
        ! apply center difference ansatz to update of j
        this%j(k,this%it_t) = this%l1 * j_m &
            - this%l2 * (this%A * ro_m + a* (ro_r - ro_l)/ this%dx )
    end do
end subroutine
!
! neighbour functions
function DRS_ro_nbrs(this,i) result(nbrs)
class(DRS) :: this
    integer :: i
    real(8),dimension(2) :: nbrs
    if(i==1) then
        nbrs(1) = this%ro(2,this%it_t)
    else
        nbrs(1) = this%ro(i-1,this%it_t)
    end if
    if(i==this%x_N) then
        nbrs(2) = this%ro(i-1,this%it_t)
    else
        nbrs(2) = this%ro(i+1,this%it_t)
    end if
end function
function DRS_j_nbrs(this,i) result(nbrs)
class(DRS) :: this
    integer :: i
    real(8),dimension(2) :: nbrs
    if(i==1) then
        nbrs(1) = this%j(1,this%it_t)
    else
        nbrs(1) = this%j(i-1,this%it_t)
    end if
    if(i==this%x_N) then
        nbrs(2) = this%j(i,this%it_t)
    else
        nbrs(2) = this%j(i+1,this%it_t)
    end if
end function
!
! file IO functions
subroutine DRS_toFile(this,FILENAME)
    class(DRS) :: this
    character(len=*):: FILENAME
    integer :: k
    open(unit = 1, file = trim(FILENAME))
    write(1,*) this%x_N, this%t_N
    write(1,*) this%x
    do k = 1,this%t_N
        write(1,*) this%ro(:,k)
    end do
    do k = 1,this%t_N
        write(1,*) this%j(:,k)
    end do
    close(1)
end subroutine
subroutine DRS_roToGnuplot(this,FILENAME)
    class(DRS) :: this
    character(len=*):: FILENAME
    integer :: k
    open(unit = 1, file = trim(FILENAME))
    do k=1,this%x_N
        write(1,*) this%x(k), this%ro(k, this%t_N-1)
    end do
end subroutine
subroutine DRS_jToGnuplot(this,FILENAME)
    class(DRS) :: this
    character(len=*):: FILENAME
    integer :: k
    open(unit = 1, file = trim(FILENAME))
    do k=1,this%x_N
        write(1,*) this%x(k), this%j(k, this%t_N-1)
    end do
end subroutine
subroutine DRS_fromFile(this,FILENAME)
    class(DRS) :: this
    character(len=*):: FILENAME
    real(8),dimension(:),allocatable :: ro_0, j_0, x_0
    real(8) :: T, eps, A, B
    integer :: x_N, t_N
    open(unit = 3, file = FILENAME, status='old')
    read(3,*) x_N, t_N
    read(3,*) T, eps, A, B
    allocate(x_0(x_N))
    allocate(ro_0(x_N))
    allocate(j_0(x_N))
    read(3,*) x_0
    read(3,*) ro_0
    read(3,*) j_0
    close(3)
    call DRS_init(this,ro_0,j_0,x_0,T,t_N,eps,A,B)
    deallocate(x_0)
    deallocate(ro_0)
    deallocate(j_0)
end subroutine
!
! free routine
subroutine DRS_free(this)
    class(DRS) :: this
    if(allocated(this%x))then
        deallocate(this%x)
    end if
    if(allocated(this%ro))then
        deallocate(this%ro)
    end if
    if(allocated(this%j))then
        deallocate(this%j)
    end if
end subroutine
end module
