function psi(eps) result(P)
	implicit none
	real(8) :: P, eps, a
	if(eps<=1) then
		P = 1
	else
		a = eps*eps
		P = 1./a
	end if
end function
