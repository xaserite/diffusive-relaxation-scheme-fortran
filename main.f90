program DRS_test
    use module_DRS
    use riemann_problem_double
    implicit none
    type(RP) :: P
    type(DRS) :: c
    character(len=*),parameter :: FILE_IN='problem.data', FILE_PARAM='../param.data', &
            FILE_OUT_RO='rho.data', FILE_OUT_J='j.data'
    integer :: x_N=400,t_N=4000
    real(8) :: eps=7e-2,T=5.e-2,A=0.,B=2,I_l=-1.,I_r=1.,ro_l=0.,ro_m=1.,ro_r=0.
	! read parameters
	open(unit=99,file=FILE_PARAM)
		read(99,*) x_N, t_N
		read(99,*) T, eps
	close(99)
	! initialize
    call P%setup(x_N,t_N,T,eps,A,B,I_l,I_r,ro_l,ro_m,ro_r)
    call P%toFile(FILE_IN)
    call c%fromFile(FILE_IN)
    call P%free
    ! calculate
    call c%compute
    call c%roToGnuplot(FILE_OUT_RO)
    !call c%jToGnuplot(FILE_OUT_J)
    call c%free
end program DRS_test
