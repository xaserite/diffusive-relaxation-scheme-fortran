module riemann_problem_double
implicit none
private
    type, public :: RP
        real(8),dimension(:),allocatable :: x, ro, j
            real(8) :: T,eps,A,B
            integer :: x_N, t_N
        contains
            procedure :: setup => RP_setup
            procedure :: free => RP_free
            procedure :: toFile => RP_toFile
    end type RP
contains
	subroutine RP_setup(this,x_N,t_N,T,eps,A,B,I_l,I_r,ro_l,ro_m,ro_r)
		class(RP) :: this
		real(8),intent(in) :: T,eps,A,B, I_l,I_r,ro_l,ro_m,ro_r
		integer, intent(in) :: x_N, t_N
		real(8) :: length, dx
		integer :: x_midNl,x_midNr, k
		length = I_r - I_l
		dx = length/(x_N-1)
		x_midNl = floor(.4*x_N)
		x_midNr = floor(.6*x_N)
		this%x_N = x_N
		this%t_N = t_N
		this%T = T
		this%eps = eps
		this%A = A
		this%B = B
		allocate(this%x(x_N))
		allocate(this%ro(x_N))
		allocate(this%j(x_N))
		do k = 1,x_midNl
			this%x(k) = I_l + (k-1)*dx
			this%ro(k) = ro_l
			this%j(k) = 0
		end do
		do k = x_midNl+1,x_midNr
			this%x(k) = I_l + (k-1)*dx
			this%ro(k) = ro_m
			this%j(k) = 0
		end do
		do k = x_midNr+1,x_N
			this%x(k) = I_l + (k-1)*dx
			this%ro(k) = ro_r
			this%j(k) = 0
		end do
	end subroutine
	subroutine RP_free(this)
		class(RP) :: this
		if(allocated(this%x))then
			deallocate(this%x)
		end if
		if(allocated(this%ro))then
			deallocate(this%ro)
		end if
		if(allocated(this%j))then
			deallocate(this%j)
		end if
	end subroutine
	subroutine RP_toFile(this,FILENAME)
		class(RP) :: this
		character(len=*) :: FILENAME
		open(unit=1,file=FILENAME)
			write(1,*) this%x_N, this%t_N
			write(1,*) this%T, this%eps, this%A, this%B
			write(1,*) this%x
			write(1,*) this%ro
			write(1,*) this%j
		close(unit=1)
	end subroutine
end module

