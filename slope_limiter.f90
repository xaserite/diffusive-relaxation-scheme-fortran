module slope_limiters
    implicit none
    private
    public :: phi
contains
    real(8) function phi(num,denom)
        real(8),intent(in) :: num,denom
        !phi = simple(num,denom)
        !phi = superbee(num,denom)
        phi = van_Leer(num,denom)
    end function
    real(8) function simple(num,denom) result(phi)
        real(8),intent(in) :: num,denom
        real(8) :: theta,m
        if(num<=0) then
            phi = 0
        elseif (denom==0) then
            phi = 1
        else
            theta = num/denom
            m = min(1.,theta)
            phi = max(0.,m)
        end if
    end function
    real(8) function superbee(num,denom) result(phi)
        real(8),intent(in) :: num,denom
        real(8) :: theta,m,n
        if(num<=0) then
            phi = 0
        elseif (denom==0) then
            phi = 2
        else
            theta = num/denom
            n = min(1.,2*theta)
            m = min(2.,theta)
            phi = max(0.,max(m,n))
        end if
    end function
    real(8) function van_Leer(num,denom) result(phi)
        real(8),intent(in) :: num,denom
        real(8) :: theta,a
        if(num<=0) then
            phi = 0
        elseif (denom==0) then
            phi = 2
        else
            theta = num/denom
            a = abs(theta)
            phi = (a+theta)/(1+a)
        end if
    end function
end module
